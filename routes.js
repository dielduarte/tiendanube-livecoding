const nextRoutes = require('next-routes');
const routes = (module.exports = nextRoutes());

routes.add('cats').add('catsCategory', '/cats/:category');
