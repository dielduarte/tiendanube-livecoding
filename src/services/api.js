import axios from 'axios';
import queryString from 'query-string';
import convert from 'xml-js';

import constants from '../constants';

function getCats(options = {}) {
  const defaultOptions = {
    results_per_page: 10,
    category: 'boxes',
    format: 'xml',
    page: 0,
    order: 'desc'
  };

  const queryParams = queryString.stringify({ ...defaultOptions, ...options });

  return axios
    .get(`${constants.API_URL}${queryParams}`)
    .then(response =>
      convert.xml2json(response.data, {
        compact: true,
        ignoreDeclaration: true,
        ignoreInstruction: true
      })
    )
    .then(responseToJson => JSON.parse(responseToJson).response.data.images.image);
}

export { getCats };
