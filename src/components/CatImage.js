import React from 'react';

function CatImage({ cat }) {
  return <img src={cat.url._text} className="col-4" style={styles.img} />;
}

const styles = {
  img: {
    objectFit: 'cover'
  }
};

export default CatImage;
