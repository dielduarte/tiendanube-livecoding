import React from 'react';
import CatImage from '../../components/CatImage';

function Ui({ catsList, isLoading }) {
  return (
    <div className="container">
      <h1>{isLoading ? 'Loading...' : 'Cats List'}</h1>

      <div className="row">
        {catsList.map(cat => (
          <CatImage cat={cat} key={cat.id._text} />
        ))}
      </div>
    </div>
  );
}

export default Ui;
