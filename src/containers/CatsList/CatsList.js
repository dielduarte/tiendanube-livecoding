import { lifecycle, compose, withState } from 'recompose';
import { withRouter } from 'next/router';

import { getCats } from '../../services/api';
import Ui from './Ui';

function getResultsPerPage(category) {
  if (category === 'space') {
    return 50;
  }

  if (category === 'hats') {
    return 20;
  }

  return 100;
}

export default compose(
  withRouter,
  withState('catsList', 'setCatsList', []),
  withState('isLoading', 'setLoading', true),
  lifecycle({
    async componentDidMount() {
      const { category } = this.props.router.query;
      const { setCatsList, setLoading } = this.props;
      const results_per_page = getResultsPerPage(category);
      const cats = await getCats({ category, results_per_page });

      setCatsList(cats);
      setLoading(false);
    }
  })
)(Ui);
