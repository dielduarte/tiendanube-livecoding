webpackHotUpdate("static/development/pages/index.js",{

/***/ "./src/components/CatImage.js":
/*!************************************!*\
  !*** ./src/components/CatImage.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


function CatImage(_ref) {
  var cat = _ref.cat;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: cat.url._text,
    className: "col-4",
    styles: styles.img
  });
}

var styles = {
  img: {
    objectFit: 'cover'
  }
};
/* harmony default export */ __webpack_exports__["default"] = (CatImage);

/***/ })

})
//# sourceMappingURL=index.js.5b0fcf16d6442f42a78e.hot-update.js.map