webpackHotUpdate("static/development/pages/index.js",{

/***/ "./src/containers/CatsList/CatsList.js":
/*!*********************************************!*\
  !*** ./src/containers/CatsList/CatsList.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var recompose__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! recompose */ "./node_modules/recompose/dist/Recompose.esm.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/api */ "./src/services/api.js");
/* harmony import */ var _Ui__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Ui */ "./src/containers/CatsList/Ui.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }






function getResultsPerPage(category) {
  if (category === 'space') {
    return 50;
  }

  if (category === 'hats') {
    return 20;
  }

  return 100;
}

/* harmony default export */ __webpack_exports__["default"] = (Object(recompose__WEBPACK_IMPORTED_MODULE_1__["compose"])(next_router__WEBPACK_IMPORTED_MODULE_2__["withRouter"], Object(recompose__WEBPACK_IMPORTED_MODULE_1__["withState"])('catsList', 'setCatsList', []), Object(recompose__WEBPACK_IMPORTED_MODULE_1__["withState"])('isLoading', 'setLoading', true), Object(recompose__WEBPACK_IMPORTED_MODULE_1__["lifecycle"])({
  componentDidMount: function () {
    var _componentDidMount = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var category, results_per_page, cats;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              category = this.props.router.query.category;
              results_per_page = getResultsPerPage(category);
              _context.next = 4;
              return Object(_services_api__WEBPACK_IMPORTED_MODULE_3__["getCats"])({
                category: category,
                results_per_page: results_per_page
              });

            case 4:
              cats = _context.sent;
              this.props.setCatsList(cats);

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function componentDidMount() {
      return _componentDidMount.apply(this, arguments);
    };
  }()
}))(_Ui__WEBPACK_IMPORTED_MODULE_4__["default"]));

/***/ })

})
//# sourceMappingURL=index.js.02835f948abcae4b5056.hot-update.js.map