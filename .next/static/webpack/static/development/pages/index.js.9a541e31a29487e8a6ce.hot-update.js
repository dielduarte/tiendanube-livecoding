webpackHotUpdate("static/development/pages/index.js",{

/***/ "./src/containers/CatsList/Ui.js":
/*!***************************************!*\
  !*** ./src/containers/CatsList/Ui.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_CatImage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/CatImage */ "./src/components/CatImage.js");



function Ui(_ref) {
  var catsList = _ref.catsList,
      isLoading = _ref.isLoading;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "Cats List"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, catsList.map(function (cat) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_CatImage__WEBPACK_IMPORTED_MODULE_1__["default"], {
      cat: cat,
      key: cat.id._text
    });
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (Ui);

/***/ })

})
//# sourceMappingURL=index.js.9a541e31a29487e8a6ce.hot-update.js.map